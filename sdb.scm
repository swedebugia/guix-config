(specifications->manifest
 '(
   ;;;;;;;;;;;;;;;;;;;;;;;;;;;
   ;; Base
   ;;;;;;;;;;;;;;;;;;;;;;;;;;;
   "android-udev-rules"
   "file"
   "glibc-utf8-locales"
   "gnupg"
   "recutils"
   "wget"
   ;; "xterm"

   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   ;; Xorg
   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   "setxkbmap"
   
   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   ;; Guix development deps :D
   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   "ccache"
   "emacs-geiser"
   ;;"emacs-guix" ;pulls in a guix with no substitute available.
   "emacs-magit"
   "emacs-markdown-mode"
   "emacs-no-x"
   "emacs-paredit"
   "emacs-rainbow-delimiters"
   ;;"emacs-gnus"
   ;;"emacs-mu4e"
   ;;"mu" ; maildir search ; pulls in xapian no sub
   "git"
   "graphviz"
   "guile-colorized"
   "guile-json"
   "guile-gcrypt"
   "guile-readline"
   "guile-sparql"
   ;;"jq"					;json multitool
   "screen"
   "tig" ; Ncurses git log viewer
   "unzip" 				;for pypi import
   
   ;;;;;;;;;;;;;;;;;;;;;
   ;; Console GUI :)
   ;;;;;;;;;;;;;;;;;;;;;
   ;; "mc" ; GNU Midnight Commander :D
   "mps-youtube"
   "rtorrent"

   ;;;;;;;;;;;;;;;;;;;;;
   ;; GUI
   ;;;;;;;;;;;;;;;;;;;;;
   ;; "dillo"
   "epiphany"
   
   ;; "gnome-screenshot"
   ;; "gnome-tweak-tool"
   ;; Gstreamer for html5 videos to work in browsers
   ;; "gst-libav"
   ;; "gst-plugins-base"
   ;; "gst-plugins-good"
   ;;"icecat" ; rust is not built yet :-/
   "network-manager-openvpn"
   "mpv"
   ;; "sbcl-next"
   ;; "tilda" ; does not work correctly in gnome
   ;; "vimb"
   "workrave"

   ;;;;;;;;;;;;;;;;;;;;;
   ;; Misc
   ;;;;;;;;;;;;;;;;;;;;;
   ;;pandoc ;html->epub conversion of manuals

   ;; Test
   ;;"youtube-viewer" ;bug: pulls in gtk
   ;;"youtube-dl-gui" wxpython takes forever to build
   ;; Missing
   ;; nautilus plugins
   ;; gedit plugins
   ;; gnome-commander
   ))
