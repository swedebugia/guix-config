# Bash initialization for interactive non-login shells and
# for remote shells (info "(bash) Bash Startup Files").

# Export 'SHELL' to child processes.  Programs such as 'screen'
# honor it and otherwise use /bin/sh.
export SHELL

if [[ $- != *i* ]]
then
    # We are being invoked from a non-interactive shell.  If this
    # is an SSH session (as in "ssh host command"), source
    # /etc/profile so we get PATH and other essential variables.
    [[ -n "$SSH_CLIENT" ]] && source /etc/profile

    # Don't do anything else.
    return
fi

# Source the system-wide file.
source /etc/bashrc

# Adjust the prompt depending on whether we're in 'guix environment'.
if [ -n "$GUIX_ENVIRONMENT" ]
then
    PS1='\u@\h \w [env]\$ '
else
    PS1='\u@\h \w\$ '
fi
alias ls='ls -p --color=auto'
alias ll='ls -l'
alias grep='grep --color=auto'

#########################
# Guix aliases
#########################
# The following is a hack to quickly search guix packages with
# grep. To update the list of packages call this function:
function setup-gp.rec () {
    # I choose these fields because it keeps the output terse and
    # readable. See the manual for a complete list of possible fields.
    # Note: tac reverses it z-a -> a-z
    guix package -s ".*" |recsel -p name,version,outputs,synopsis \
	| tac >~/gp.rec

    # Inform the user about number of packages in the file:
    echo "Total number of packages cached in gp.rec:"
    cat ~/gp.rec |recsel -p name |grep -v '^$'|wc -l    
}
alias g="grepgp" #grep guix package ;-)
alias grepgp="grep -i -3 ~/gp.rec -e" #good readability not recsel
#compatible

alias grepgpr="grep -i ~/gp.rec -e" #recsel compatible

# The usual suspects :)
alias ga="guix archive"
alias gb="guix build"
alias gch="guix challenge"
alias gcon="guix container"
alias gcop="guix copy"
alias gcopy="guix copy"
alias gde="guix describe"
alias gdo="guix download"
alias gdown="guix download"
alias ged="guix edit"
alias gen="guix environment"
alias genv="guix environment"
alias ggc="guix gc"
alias ggr="guix graph"
alias gh="guix hash"
alias gi="guix import"
alias gl="guix lint"
alias gpack="guix pack"
alias gp="guix package"

# Guix package show
function s () {
    #  show package
    guix package --show=$1
    }
alias gps="s" #guix package show
alias gpr="guix processes"
alias gpub="guix publish"
alias gpull="guix pull"
alias gref="guix refresh"
alias grepl="guix repl"

alias gs="guix system"
alias gsi="guix size"
alias gw="guix weather"

# Append to history after each command
shopt -s histappend
PROMPT_COMMAND="history -a;$PROMPT_COMMAND"

# Welcome reminder
echo "# Remember: 
# s = human readable guix package --show
# g = grepgp = fast search of package list + easy to read and filter
# gb = guix build
# gp = guix package
# gpr = guix package -s (recsel compatible)
# gs = guix system
" 

# Utility functions
function guix-renice () {
	 #renice the daemon and children
	 sudo herd status guix-daemon|grep value| gawk '{print $4}'|sed 's/\.//'|xargs sudo renice -19
	 guix processes|recsel -p SessionPID,ClientPID,ChildProcess|gawk '{ print $2 }'|sed 's/://'|xargs sudo renice -19
    }
