;; This is an operating system configuration template
;; for a "desktop" setup with GNOME and Xfce where the
;; root partition is encrypted with LUKS.

(use-modules (gnu) (gnu system nss))
(use-service-modules desktop networking virtualization)
(use-package-modules certs gnome
		     ;; for my-base-packages
		     linux less base nano pciutils admin man texinfo
		     bash guile gawk compression lisp)

(define %my-base-packages
  ;; Like upstream but without wifi-programs and zile.

  ;; Default set of packages globally visible.  It should include anything
  ;; required for basic administrator tasks.
  (cons* procps psmisc which less
	 ;;zile
	 nano
         pciutils usbutils
         util-linux
         inetutils isc-dhcp
         (@ (gnu packages admin) shadow)          ;for 'passwd'

         ;; wireless-tools is deprecated in favor of iw, but it's still what
         ;; many people are familiar with, so keep it around.
         ;;iw wireless-tools

         iproute
         net-tools                        ; XXX: remove when Inetutils suffices
         man-db
         info-reader                     ;the standalone Info reader (no Perl)

         ;; The 'sudo' command is already in %SETUID-PROGRAMS, but we also
         ;; want the other commands and the man pages (notably because
         ;; auto-completion in Emacs shell relies on man pages.)
         sudo

         ;; Get 'insmod' & co. from kmod, not module-init-tools, since udev
         ;; already depends on it anyway.
         kmod eudev

         e2fsprogs kbd

         bash-completion

         ;; XXX: We don't use (canonical-package guile-2.2) here because that
         ;; would create a collision in the global profile between the GMP
         ;; variant propagated by 'guile-final' and the GMP variant propagated
         ;; by 'gnutls', itself propagated by 'guix'.
         guile-2.2

         ;; The packages below are also in %FINAL-INPUTS, so take them from
         ;; there to avoid duplication.
         (map canonical-package
              (list bash coreutils findutils grep sed
                    diffutils patch gawk tar gzip bzip2 xz lzip))))

(operating-system
  (host-name "antelope")
  (timezone "Europe/Paris")
  (locale "en_US.utf8")

  ;; Use the UEFI variant of GRUB with the EFI System
  ;; Partition mounted on /boot/efi.
  (bootloader (bootloader-configuration
                (bootloader grub-bootloader)
		(target "/dev/sda")))

  (file-systems (cons (file-system
                        (device (file-system-label "my-root"))
                        (mount-point "/")
                        (type "ext4"))
		      %base-file-systems))

  (swap-devices '("/dev/sda2"))

  (users (cons (user-account
                (name "sdb")
                (comment "Alice's brother")
                (group "users")
                (supplementary-groups '("wheel" "netdev"
                                        "audio" "video"))
                (home-directory "/home/sdb"))
               %base-user-accounts))

  ;; This is where we specify system-wide packages.
  (packages (cons* nss-certs         ;for HTTPS access
                   gvfs              ;for user mounts
		   
		   network-manager-openvpn
		   stumpwm
                   %my-base-packages))

  ;; Add GNOME and/or Xfce---we can choose at the log-in
  ;; screen with F1.  Use the "desktop" services, which
  ;; include the X11 log-in service, networking with
  ;; NetworkManager, and more.
  (services (cons* (service qemu-binfmt-service-type
                     (qemu-binfmt-configuration
                       (platforms (lookup-qemu-platforms "arm"))
                       (guix-support? #t)))
		   (xfce-desktop-service)
		   (console-keymap-service "se-latin1")
                   (modify-services %desktop-services      ; Add berlin as first priority 
                                                           ; substitute server
                                    (guix-service-type config =>
                                                       (guix-configuration
                                                        (inherit config)
                                                        (substitute-urls '(
									   "https://berlin.guixsd.org https://mirror.hydra.gnu.org"))))
				    (network-manager-service-type config =>
								  (network-manager-configuration
								   (inherit config)
								   (vpn-plugins (list network-manager-openvpn)))))))

  ;; Allow resolution of '.local' host names with mDNS.
  ;;(name-service-switch %mdns-host-lookup-nss)
  )
